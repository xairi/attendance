from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _
from personal.models import Trabajador


def non_free_email(value):
    DOMAIN_BLACKLIST = (
        'gmail.com',
        'hotmail.com',
        'outlook.com'
    )
    user, domain = value.split('@')
    if domain in DOMAIN_BLACKLIST:
        message = _(u'No aceptamos correos de %s' % domain)
        raise ValidationError(message)


def code_validation(value):
    if not Trabajador.objects.filter(pk=value).exists():
        raise ValidationError('Usuario inexistente')


